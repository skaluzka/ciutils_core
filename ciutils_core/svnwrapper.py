import subprocess
import sys

# import stuff from ciutils_core package
from const import RET_CODE_ERROR
from const import RET_CODE_SUCCESS


class SvnWrapper(object):


    def __init__(self):
        pass


    @staticmethod
    def svn_cmd(cmd_str, std_out_file=None, std_err_file=None):
        print 'cmd_str = "%s"' % cmd_str
        proc = subprocess.Popen(cmd_str,
                                stdout=subprocess.PIPE,
                                stderr=subprocess.PIPE,
                                shell=True)
        std_out, std_err = proc.communicate()
        if std_out_file:
            with open(std_out_file, 'w') as f:
                f.write(std_out)
        else:
            print std_out
        if std_err_file:
            with open(std_err_file, 'w') as f:
                f.write(std_err)
        else:
            print std_err
        if proc.returncode:
            print 'std_out:', std_out
            print 'std_err:', std_err
            return RET_CODE_ERROR
        return RET_CODE_SUCCESS


    def svn_version(self):
        self.svn_cmd('svn --version')




def main():
    sw = SvnWrapper()
    sw.svn_version()
    sw.svn_cmd('svn --version', std_out_file='svn_version.txt')
    sw.svn_cmd('svn info .')
    sw.svn_cmd('svn st .', std_out_file='stdout.log', std_err_file='stderr.log')




if __name__ == '__main__':
    sys.exit(main())

#todo:
# - logging module should be added
# - time execution decorator could be added
# - docstrings are missing

