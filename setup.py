#!/bin/env python2


from ciutils_core import version
from setuptools import setup


setup_dict = {
    "name": "ciutils_core",
    "version": version.__version__,
    "description": "",
    "zip_safe": False,
    "packages": ["ciutils_core"],
    "extras_require": {
        "dev": [
            "pylint",
            "pytest",
            "radon",
            "sphinx",
        ]
    },
    "scripts": [
        "ciutils_core/env.sh",
        "ciutils_core/basefunc1.sh",
        "ciutils_core/basefunc2.sh",
    ],
}

setup(**setup_dict)

