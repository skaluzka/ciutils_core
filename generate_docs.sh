#!/bin/bash
set -e

DOCS_DIR=docs/


if [ -d ${DOCS_DIR} ] ; then
    echo "removing ${DOCS_DIR} please wait..."
    rm -rf docs/
    echo "dir ${DOCS_DIR} removed successfully"
fi

CIUTILS_CORE_VER=$(grep __version__ ciutils_core/version.py | cut -d"=" -f2 | tr -d " ")
echo "CIUTILS_CORE_VER=${CIUTILS_CORE_VER}"

echo "configuring sphinx, please wait..."
sphinx-apidoc -A "skaluzka@riseup.net" -H "ciutils_core" -V ${CIUTILS_CORE_VER} -e -F -o ${DOCS_DIR} ciutils_core/
cd ${DOCS_DIR}
echo "generating *.html documentation, please wait..."
make html
echo "*.html documentation generated successfully"

echo "generating *.pdf documentation, please wait..."
make latexpdf
echo "*.pdf documentation generated successfully"

echo "done"

