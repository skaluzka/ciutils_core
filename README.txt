1. installation from bitbucket repository with pip


if you want to install ciutils_core package directly from bitbucket repository
please run one of below commands (pip is required).

for bleeding-edge version please type:


    pip install https://bitbucket.org/skaluzka/ciutils_core/get/master.tar.gz


for specific version installation (0.5.0) please type:


    pip install https://bitbucket.org/skaluzka/env_checker/get/v0.5.0.tar.gz


after run above commands the following directories will be created in your
site-packages/ dir: ciutils_core/, ciutils_core-0.5.0-py2.7.egg-info/
no additional packages will be installed


2. installation from local dir with pip


this step assume that you have downloaded and unpacked source files for
ciutils_core package. also pip tool is required to perform the installation. to
install the package in your system just change directory to that one which
contains setup.py script and simply type:


    pip install .


after run above command the following directories will be created in your
site-packages/ dir: ciutils_core/, ciutils_core-0.5.0-py2.7.egg-info/
no additional packages will be installed


3. installation from local dir with setuptools


this step assume you have downloaded and unpacked source files for ciutils_core
package. for install please change directory to that one which contains
setup.py script and type:


    python setup.py install


after run above command the following directories will be created in your
site-packages/ dir: ciutils_core/, ciutils_core-0.5.0-py2.7.egg-info/
no additional packages will be installed


4. development installation from bitbucket repository with pip


you can also install develop version of ciutils_core package with below command
(if you have pip tool available in your system):


    pip install https://bitbucket.org/skaluzka/ciutils_core/get/master.tar.gz -e .[dev]


this time not only ciutils_core package will be installed in your
site-packages/ dir but also all packages specified in extras_require
list in setup.py script. please be aware that dependencies for those
packages will be installed as well therefore the whole development setup
requires more space on your disk


5.  development installation from local dir with pip


this step assume you have downloaded and unpacked ciutils_core package's source
files. installing development version from local dir is very easy with pip tool.
change the directory to that one which contains setup.py script and run
below command:


    pip install -e .[dev]


please be aware in this case ciutils_core package will be not installed in
your site-packages/ dir but there will be an ciutils-core.egg-link file
created inside. all packages specified in extras_require list in
setup.py script and their dependencies will be installed as well. therefore
the whole development setup requires more space on your disk

